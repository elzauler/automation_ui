import calendar
import time
import unittest

from selenium import webdriver
import pytest
from pageObjects.LoginPage import LoginPage
from utilities.logger import LogGen


@pytest.mark.run(order=2)
class Test_Login(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(100)
        self.logger = LogGen.loggen()
        self.ts = calendar.timegm(time.gmtime())
        self.logger.info("=== Test_Login ==")

    @classmethod
    def setUpClass(cls):
        cls.baseURL = "http://192.168.1.252:8080/auth/realms/flo-realm/protocol/openid-connect/auth?client_id=flo" \
                      "-logic&redirect_uri=http%3A%2F%2Fdocker-flo-qa.bdinfra.net%3A85%2F%23%2Fdashboard&state" \
                      "=f9b55df6-2531-4011-878d-9625f005de92&response_mode=fragment&response_type=code&scope=openid" \
                      "&nonce=21cf1943-e761-4a6f-8533-905a6624b1ff "
        cls.username = "elr2d2@walla1.com"
        cls.password = "1234"

    def test_home_page_title(self):
        self.logger.info("*** test_home_page_title ***")
        self.logger.info("*** validates home page title ***")
        self.driver.get(self.baseURL)
        act_title = self.driver.title

        if act_title == "Log in to flo-realm":
            self.logger.info("### test_home_page_title has passed (: ###")
            assert True
        else:
            self.logger.info("!!! test_home_page_title has failed ): !!!")
            self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_home_page_title.png")
            assert False

    def test_login(self):
        self.logger.info("*** test_login ***")
        self.logger.info("*** validates login ***")
        self.driver.get(self.baseURL)
        self.loginPage = LoginPage(self.driver)

        self.loginPage.set_username(self.username)
        self.loginPage.set_password(self.password)
        self.loginPage.click_login()
        act_title = self.driver.title

        if act_title == "McClientUi":
            self.logger.info("### test_login has passed (: ###")
            assert True
        else:
            self.logger.info("!!! test_login failed ): !!!")
            self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_login.png")
            assert False

    def test_login_invalid_password(self):
        self.logger.info("*** test_login_invalid_password ***")
        self.logger.info("*** validates password ***")
        self.password = "users"
        self.driver.get(self.baseURL)
        self.loginPage = LoginPage(self.driver)

        self.loginPage.set_username(self.username)
        self.loginPage.set_password(self.password)
        self.loginPage.click_login()
        act_title = self.driver.title

        if act_title != "McClientUi":
            self.logger.info("### test_home_page_title has passed (: ###")
            assert True
        else:
            self.logger.info("!!! test_login failed ): !!!")
            self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_login_invalid_password.png")
            assert False

    def test_login_invalid_username(self):
        self.logger.info("*** test_login_invalid_username ***")
        self.logger.info("*** validates username ***")
        self.username = "users"
        self.driver.get(self.baseURL)
        self.loginPage = LoginPage(self.driver)

        self.loginPage.set_username(self.username)
        self.loginPage.set_password(self.password)
        self.loginPage.click_login()
        act_title = self.driver.title

        if act_title != "McClientUi":
            self.logger.info("### test_home_page_title has passed (: ###")
            assert True
        else:
            self.logger.info("!!! test_login failed ): !!!")
            self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_login_invalid_username.png")
            assert False

    def test_login_invalid_example(self):
        self.logger.info("*** test_login_invalid_example ***")
        self.logger.info("*** example of failure ***")
        self.username = "users"
        self.driver.get(self.baseURL)
        self.loginPage = LoginPage(self.driver)

        self.loginPage.set_username(self.username)
        self.loginPage.set_password(self.password)
        self.loginPage.click_login()
        act_title = self.driver.title

        if act_title == "McClientUi":
            self.logger.info("!!! test_login_invalid_example has passed ): !!!")
            assert True
        else:
            self.logger.info("### test_login_invalid_example has failed (: ###")
            self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_login_invalid_example.png")
            assert False

    @classmethod
    def tearDownClass(cls):
        pass

    def tearDown(self):
        self.driver.close()
        self.logger.info("=== Test_Login ==")

# test login without password/username
