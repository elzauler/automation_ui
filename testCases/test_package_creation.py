import random
import time
import unittest
import calendar
import pytest
from selenium import webdriver
from utilities.logger import LogGen
import null
from pageObjects.LoginPage import LoginPage
from pageObjects.PackagePage import PackagePage


@pytest.mark.run(order=1)
class Test_Package(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(3000)
        self.logger = LogGen.loggen()
        self.ts = calendar.timegm(time.gmtime())
        self.logger.info("=== Test_Package_Creation ==")

    @classmethod
    def setUpClass(cls):
        cls.rand_num = random.randrange(100, 5000, 10)
        cls.package_type = ["USAGE", "MONEY", "RATE"]
        cls.package_cost = random.randrange(100, 5000, 10)
        cls.data_allowance = random.randrange(100, 5000, 10)
        cls.data_type = ["KB", "MB", "GB"]
        cls.sms_unit = random.randrange(100, 5000, 10)
        cls.renewal_interval = ["DAYS", "WEEKS", "MONTHS", "YEARS"]
        cls.currency_type = "USD"

        cls.usage_package_name = cls.package_type[0] + " " + str(cls.package_cost) + " " + cls.data_type[
            random.randrange(0, 2)]
        cls.money_package_name = cls.package_type[1] + " " + str(cls.package_cost) + " " + cls.data_type[
            random.randrange(0, 2)]
        cls.rate_package_name = cls.package_type[2] + " " + str(cls.package_cost) + " " + cls.data_type[
            random.randrange(0, 2)]

        cls.usage_package = {'package_type': cls.package_type[0],
                             'package_cost': cls.package_cost,
                             'data_allowance': cls.data_allowance,
                             'data_type': cls.data_type[random.randint(0, 2)],
                             'currency_type': cls.currency_type,
                             'sms_unit': cls.sms_unit,
                             'renewal_interval': cls.renewal_interval[random.randint(0, 3)]}

        cls.money_package = {'package_type': cls.package_type[1],
                             'package_cost': cls.package_cost,
                             'data_allowance': cls.data_allowance,
                             'data_type': cls.data_type[random.randint(0, 2)],
                             'currency_type': cls.currency_type,
                             'sms_unit': cls.sms_unit,
                             'renewal_interval': cls.renewal_interval[random.randint(0, 3)]}

        cls.rate_package = {'package_type': cls.package_type[2],
                            'package_cost': cls.package_cost,
                            'data_allowance': cls.data_allowance,
                            'data_type': cls.data_type[random.randint(0, 2)],
                            'currency_type': cls.currency_type,
                            'sms_unit': cls.sms_unit,
                            'renewal_interval': cls.renewal_interval[random.randint(0, 3)]}

        cls.baseURL = "http://192.168.1.252:8080/auth/realms/flo-realm/protocol/openid-connect/auth?client_id=flo" \
                      "-logic&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2F%23%2Fbilling%2Fbilling-plans&state" \
                      "=6bb357ed-4c66-4050-8f93-c0defd817db0&response_mode=fragment&response_type=code&scope=openid" \
                      "&nonce=6002d2ea-a18e-4864-86a8-a5ccc2b2c670 "
        cls.username = "elr2d2@walla1.com"
        cls.password = "1234"
        cls.currentURL = "http://localhost:4200/#/billing/billing-plans"

    def test_create_usage_package(self):
        self.logging_in()
        time.sleep(3)
        package_name = str(str(random.randint(1000, 99999)) + "_" + self.usage_package['package_type'] + "-" + str(
            self.usage_package['package_cost']) +
                           self.usage_package['data_type'])

        self.package_page = PackagePage(self.driver)

        if self.driver.current_url == self.currentURL:
            self.package_page.click_create_new_package()
            time.sleep(1)
            self.package_page.set_package_name(package_name)
            time.sleep(1)
            self.package_page.set_package_type(self.usage_package['package_type'])
            time.sleep(1)
            self.package_page.set_package_cost(self.usage_package['package_cost'], self.usage_package['currency_type'])
            time.sleep(1)
            self.package_page.set_data(self.usage_package['data_allowance'], self.usage_package['data_type'])
            time.sleep(1)
            self.package_page.set_sms(self.usage_package['sms_unit'])
            time.sleep(1)
            self.package_page.set_renewal_interval(self.usage_package['renewal_interval'])
            time.sleep(1)
            self.package_page.click_create()
            time.sleep(5)

            if not (self.package_page.is_billing_plan_exist() and self.package_page.is_billing_plan_in_database()):
                print("No element found")
                self.logger.info("!!! test_package_creation failed ): !!!")
                self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_package_creation.png")
                assert False
            assert True

    def test_create_money_package(self):
        self.logging_in()
        time.sleep(3)
        package_name = str(str(random.randint(1000, 99999)) + "_" + self.money_package['package_type'] + "-" + str(
            self.money_package['package_cost']) +
                           self.money_package['data_type'])
        self.package_page = PackagePage(self.driver)

        if self.driver.current_url == self.currentURL:
            self.package_page.click_create_new_package()
            time.sleep(1)
            self.package_page.set_package_name(package_name)
            time.sleep(1)
            self.package_page.set_package_type(self.money_package['package_type'])
            time.sleep(1)
            self.package_page.set_balance(self.money_package['package_cost'], self.money_package['currency_type'])
            time.sleep(1)
            self.package_page.set_renewal_interval(self.money_package['renewal_interval'])
            time.sleep(1)
            self.package_page.click_create()
            time.sleep(5)

            if not (self.package_page.is_billing_plan_exist() and self.package_page.is_billing_plan_in_database()):
                print("No element found")
                self.logger.info("!!! test_package_creation failed ): !!!")
                self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_package_creation.png")
                assert False
            assert True

    def test_create_rate_package(self):
        self.logging_in()
        time.sleep(3)
        package_name = str(str(random.randint(1000, 99999)) + "_" + self.rate_package['package_type'] + "-" + str(
            self.rate_package['package_cost']) +
                           self.rate_package['data_type'])
        self.package_page = PackagePage(self.driver)

        if self.driver.current_url == self.currentURL:
            self.package_page.click_create_new_package()
            time.sleep(1)
            self.package_page.set_package_name(package_name)
            time.sleep(1)
            self.package_page.set_package_type(self.rate_package['package_type'])
            time.sleep(1)
            self.package_page.set_renewal_interval(self.rate_package['renewal_interval'])
            time.sleep(1)
            self.package_page.set_rate_data(self.rate_package['data_allowance'], self.rate_package['data_type'])
            time.sleep(1)
            self.package_page.set_rate_sms(self.rate_package['sms_unit'])
            time.sleep(1)
            self.package_page.click_create()
            time.sleep(5)

            if not (self.package_page.is_billing_plan_exist() and self.package_page.is_billing_plan_in_database()):
                print("No element found")
                self.logger.info("!!! test_package_creation failed ): !!!")
                self.driver.save_screenshot(".\\Screenshots\\" + str(self.ts) + "_test_package_creation.png")
                assert False
            assert True

    @classmethod
    def tearDownClass(cls):
        cls.package_name = null
        cls.package_type = null
        cls.package_cost = null
        cls.data_allowance = null
        cls.data_type = null
        cls.sms_unit = null
        cls.renewalInterval = null

    def tearDown(self):
        self.driver.close()
        self.driver.quit()
        self.logger.info("=== Test_Package_Creation ==")

    def logging_in(self):
        self.driver.get(self.baseURL)
        self.loginPage = LoginPage(self.driver)
        self.loginPage.set_username(self.username)
        self.loginPage.set_password(self.password)
        self.loginPage.click_login()
