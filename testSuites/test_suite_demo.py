import unittest

from testCases.test_login import Test_Login
from testCases.test_package_creation import Test_Package

# Get all tests from the test classes
testLoader = unittest.TestLoader()

sanity = [
    testLoader.loadTestsFromTestCase(Test_Login), testLoader.loadTestsFromTestCase(Test_Package)
]

# Create a test suite combining all test classes
unittest.TestSuite(sanity)
# regressionTest = unittest.TestSuite([tc2, tc1])
