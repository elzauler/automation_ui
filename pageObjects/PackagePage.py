import null
import requests


def req_logging_in():
    url = 'http://192.168.1.252:8080/auth/realms/flo-realm/protocol/openid-connect/token'

    payload = {
        "password": "root",
        "username": "root",
        "grant_type": "password",
        "client_id": "public-api",
        "client_secret": "cc2fa18e-6458-44d4-a500-d912d460f263"
    }

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    j_response = response.json()['access_token']
    return str(j_response)


def req_get_user_billing_plans():
    url = "http://docker-flo-qa.bdinfra.net:8888/api/public-api/billing-plan-by-account/0/100/5ba39f3b-29d2-43ba-948e-0b7103ed0740"

    headers = {
        'Authorization': 'Bearer ' + req_logging_in(),
        'Cookie': 'JSESSIONID=3C819AD55EEB183C17B8EEB5684B73F6'
    }

    response = requests.request("GET", url, headers=headers)
    j_response = response.json()
    return str(j_response)


class PackagePage:
    button_billing_plan_xpath = "//*[@id='cdk-accordion-child-0']/div/app-sidenav-submenu[2]/mat-nav-list/a/div"
    listbox_billing_xpath = "//*[@id='billing']"

    button_create_new_package_xpath = "//*[@id='create-new-package']"
    textbox_package_name_id = "package-name"
    listbox_package_type_xpath = "//*[@id='package-type']"
    textbox_package_cost_xpath = "//*[@id='package-cost']"
    listbox_currency_xpath = "//*[@id='select-currency']"

    checkbox_data_xpath = "//*[@id='data-checkbox']"
    checkbox_sms_xpath = "//*[@id='sms-checkbox']"
    textbox_usage_allowance_id = "usage-data-allowance"
    textbox_rate_allowance_id = "rate-data-allowance"
    textbox_balance_value_id = "balance-value"
    listbox_usage_data_type_xpath = "//*[@id='usage-data-unit']"
    listbox_rate_data_type_xpath = "//*[@id='rate-data-unit']"
    textbox_usage_sms_unit_xpath = "//*[@id='usage-sms-units']"
    textbox_rate_sms_unit_xpath = "//*[@id='rate-sms-units']"

    listbox_renewal_interval_xpath = "//*[@id='renewalInterval']"
    package_name = null
    button_create_id = "create-button"
    button_cancel_id = "cancel-button"
    billing_plans_list_id = "billing-plan-list"

    def __init__(self, driver):
        self.driver = driver

    def set_package_name(self, package_name):
        self.package_name = package_name
        self.driver.find_element_by_id(self.textbox_package_name_id).send_keys(package_name)

    def set_package_type(self, pack_type):
        self.driver.find_element_by_xpath(self.listbox_package_type_xpath).click()
        self.driver.find_element_by_id(pack_type).click()

    def set_package_cost(self, package_cost, currency_type):
        self.driver.find_element_by_xpath(self.textbox_package_cost_xpath).send_keys(package_cost)
        self.driver.find_element_by_xpath(self.listbox_currency_xpath).click()
        self.driver.find_element_by_id(currency_type).click()

    def set_data(self, allowance, data_type):
        self.driver.find_element_by_id(self.textbox_usage_allowance_id).send_keys(allowance)
        self.driver.find_element_by_xpath(self.listbox_usage_data_type_xpath).click()
        self.driver.find_element_by_id(data_type).click()
        self.driver.find_element_by_xpath(self.checkbox_data_xpath).click()

    def set_rate_data(self, allowance, data_type):
        self.driver.find_element_by_id(self.textbox_rate_allowance_id).send_keys(allowance)
        self.driver.find_element_by_xpath(self.listbox_rate_data_type_xpath).click()
        self.driver.find_element_by_id(data_type).click()

    def set_balance(self, balance, currency_type):
        self.driver.find_element_by_id(self.textbox_balance_value_id).send_keys(balance)
        self.driver.find_element_by_xpath(self.listbox_currency_xpath).click()
        self.driver.find_element_by_id(currency_type).click()

    def set_sms(self, sms_unit):
        self.driver.find_element_by_xpath(self.textbox_usage_sms_unit_xpath).send_keys(sms_unit)
        self.driver.find_element_by_xpath(self.checkbox_sms_xpath).click()

    def set_rate_sms(self, sms_unit):
        self.driver.find_element_by_xpath(self.textbox_rate_sms_unit_xpath).send_keys(sms_unit)

    def set_renewal_interval(self, renewal_type):
        self.driver.find_element_by_xpath(self.listbox_renewal_interval_xpath).click()
        self.driver.find_element_by_id(renewal_type).click()

    def click_create(self):
        self.driver.find_element_by_id(self.button_create_id).click()

    def click_cancel(self):
        self.driver.find_element_by_id(self.button_cancel_id).click()

    def click_create_new_package(self):
        self.driver.find_element_by_xpath(self.button_create_new_package_xpath).click()

    def click_billing_plan(self):
        self.driver.find_element_by_xpath(self.button_billing_plan_xpath).click()

    def click_billing(self):
        self.driver.find_element_by_xpath(self.listbox_billing_xpath).click()

    def get_package_name(self):
        return self.package_name

    def is_billing_plan_exist(self):
        if not self.driver.find_element_by_xpath("//*[@id='{}']".format(self.get_package_name())) == null:
            return True
        return False

    def is_billing_plan_in_database(self):
        package_name = self.get_package_name()
        billing_plan_list = req_get_user_billing_plans()
        print("\n\n\n===================================================\n\n\n"+billing_plan_list+"\n\n\n===================================================\n\n\n")
        if package_name in billing_plan_list:
            return True
        return False
