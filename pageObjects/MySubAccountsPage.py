import null
import requests


def req_logging_in():
    url = "http://192.168.1.252:8080/auth/realms/flo-realm/protocol/openid-connect/token"

    payload = 'password=root&username=root&grant_type=password&client_id=public-api&client_secret=cc2fa18e-6458-44d4-a500-d912d460f263'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    j_response = response.json()['access_token']
    return str(j_response)


def req_get_user_billing_plans():
    url = "http://docker-flo-qa.bdinfra.net:8888/api/public-api/billing-plan-by-account/0/100/5ba39f3b-29d2-43ba-948e-0b7103ed0740"
    headers = {
        'Authorization': 'Bearer ' + req_logging_in(),
        'Cookie': 'JSESSIONID=3C819AD55EEB183C17B8EEB5684B73F6'
    }

    response = requests.request("GET", url, headers=headers)
    j_response = response.json()
    return str(j_response)


class MySubAccountsPage:
    button_create_new_account = "//*[@id='create-new-account']"
    textbox_sub_account_name = "//*[@id='sub_account-name']"
    listbox_sub_account_type = "//*[@id='sub_account-type']"



    def __init__(self, driver):
        self.driver = driver
