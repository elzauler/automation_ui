class LoginPage:
    textbox_username_id = "username"
    textbox_password_id = "password"
    button_login_xpath = "//*[@id='kc-login']"
    button_logout_xpath = "//*[@id='mat-menu-panel-0']/div/button[1]"

    def __init__(self, driver):
        self.driver = driver

    def set_username(self, username):
        self.driver.find_element_by_id(self.textbox_username_id).send_keys(username)

    def set_password(self, password):
        self.driver.find_element_by_id(self.textbox_password_id).send_keys(password)

    def click_login(self):
        self.driver.find_element_by_xpath(self.button_login_xpath).click()

    def click_logout(self):
        self.driver.find_element_by_xpath(self.button_logout_xpath).click()

''' 
 login_dict = {
        "baseURL": "",
        "username": "",
        "password": "",
    }
       
    def set_login_dict(self, baseURL, username, password):
        self.login_dict["baseURL"] = baseURL
        self.login_dict["username"] = username
        self.login_dict["password"] = password

    def get_login_dict(self):
        return self.login_dict
'''